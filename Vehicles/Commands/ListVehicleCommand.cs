﻿
using Madin_Challenge___Vehicles.Models;


namespace Vehicles.Commands
{
    public class ListVehicleCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (args.Length == 2)
            {
                return Actions.ListVehicles(args[1]);
            }
            return Actions.ListVehicles();
        }
    }
}