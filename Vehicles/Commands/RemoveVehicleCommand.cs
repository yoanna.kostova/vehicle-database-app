﻿
using Madin_Challenge___Vehicles.Models;
using Vehicles.Commands;

namespace Madin_Challenge___Vehicles.Commands
{
    public class RemoveVehicleCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (args.Length != 2)
            {
                return "Please provide at least 2 arguments. The first argument should be a command, the second - serial number";
            }
            return Actions.RemoveVehicle(args[1]);
        }
    }
}
