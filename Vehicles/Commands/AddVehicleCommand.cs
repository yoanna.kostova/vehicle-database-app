﻿using Madin_Challenge___Vehicles.Infrastructure;
using Madin_Challenge___Vehicles.Models;
using System;
using Vehicles.Commands;

namespace Madin_Challenge___Vehicles.Commands
{

    public class AddVehicleCommand : ICommand
    {
        public string Execute(string[] args)
        {
            
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide at least 3 arguments. Type of vehicle (Car, Boat, Bike), Model and Color");
            }
            switch (args[1])
            {
                case "Car":
                    {
                        Car newVehicle = new Car(args[2], args[3]);
                        return Actions.AddVehicle(newVehicle);

                    }
                case "Bike":
                    {
                        Bike newVehicle = new Bike(args[2], args[3]);
                        return Actions.AddVehicle(newVehicle);

                    }
                case "Boat":
                    {
                        Boat newVehicle = new Boat(args[2], args[3]);
                        return Actions.AddVehicle(newVehicle);

                    }
                default:
                    return "Please provide valid vehicle type.";
            }
        }
    }
}
