﻿using Madin_Challenge___Vehicles.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles.Commands
{
    public class UpdateVehicleCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (args.Length!=4)
            {
                return "Please provide valid input - command, vehicle serial number, \"color\"/\"model\", new color/model.";
            }
            return Actions.UpdateVehicle(args[1], args[2], args[3]);
        }
    }
}
