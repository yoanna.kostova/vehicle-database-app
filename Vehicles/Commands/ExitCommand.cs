﻿
using Vehicles.Commands;

namespace Madin_Challenge___Vehicles.Commands
{
    public class ExitCommand : ICommand
    {
        public string Execute(string[] args)
        {
            return "Have a good one!";
        }
    }
}
