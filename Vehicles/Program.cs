﻿using Madin_Challenge___Vehicles.Infrastructure;
using Madin_Challenge___Vehicles.IO_Engine;
using System;

namespace Madin_Challenge___Vehicles
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine engine = new Engine(new ConsoleIOEngine());
            Engine newEngine = new Engine(new FileIOEngine());
            engine.Run();
        }
    }
}
