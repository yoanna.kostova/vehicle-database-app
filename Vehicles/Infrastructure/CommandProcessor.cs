﻿using Madin_Challenge___Vehicles.Commands;
using System.Collections.Generic;
using Vehicles.Commands;

namespace Madin_Challenge___Vehicles.Infrastructure
{
    public class CommandProcessor
    {
        public static string Process(string[] args)
        {
            Dictionary<string, ICommand> commandsDict = new Dictionary<string, ICommand>()
            {
                {"1", new AddVehicleCommand() },
                {"2", new ListVehicleCommand()  },
                {"3", new RemoveVehicleCommand() },
                {"4", new UpdateVehicleCommand() },
                {"exit", new ExitCommand() }

            };
            if (!commandsDict.ContainsKey(args[0]))
            {
                return "Please provide a valid command.";
            }

            var item1 = commandsDict["1"];
           
            return commandsDict[args[0]].Execute(args);    
        }
    }
}
