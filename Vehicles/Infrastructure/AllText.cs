﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles.Infrastructure
{
    public static class AllText
    {
        public const string welcome = "Welcome to our application";
        public const string AddVehicle = "Please provide at least 3 arguments. Type of vehicle (Car, Boat, Bike), Model and Color";
    }
}
