﻿using Madin_Challenge___Vehicles.IO_Engine;
using System;
using System.Linq;

namespace Madin_Challenge___Vehicles.Infrastructure
{
    public class Engine
    {
        private readonly IIOEngine ioEngine;

        public Engine(IIOEngine ioEngine)
        {
            this.ioEngine = ioEngine;
        }
         public void Run()
        {
            ioEngine.Write(AllText.welcome);
            ioEngine.Write($"1 - To ADD a vehicle");
            ioEngine.Write(AllText.AddVehicle);
            ioEngine.Write("2 - To PRINT all vehicles");
            ioEngine.Write("2 -  and include the serial number to PRINT a certain vehicle");
            ioEngine.Write("3 - To REMOVE a vehicle");
            ioEngine.Write("4 - and add a serial number to UPDATE a vehicle");
            ioEngine.Write("exit - To EXIT the application");

            
            while (true)
            {
                string[] args = ioEngine.Read().Trim().Split().Select(x => x.Trim()).ToArray();
                if (args.Length > 0)
                {
                    string result = CommandProcessor.Process(args);
                    ioEngine.Write(result);
                    if (args[0] == "exit" )
                    {
                        break;
                    }
                }
            }
        }
    }
}
