﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles.IO_Engine
{
    public interface IIOEngine
    {
        public string Read();
        public void Write(string str);
    }
}
