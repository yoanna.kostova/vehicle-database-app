﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles.IO_Engine
{
    public class ConsoleIOEngine : IIOEngine
    {
        public string Read()
        {
            return Console.ReadLine();
        }

        public void Write(string str)
        {
            Console.WriteLine(str);
        }
    }
}
