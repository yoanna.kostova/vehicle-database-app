﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Madin_Challenge___Vehicles.Models
{
    public static class Actions
    {
        //Fields
        private static List<Vehicle> vehicleList = new List<Vehicle>();

        //Methods
        public static string AddVehicle(Vehicle vehicle)
        {
            if (vehicleList.Contains(vehicle))
            {
                return "This car already exists in our database.";
            }
            else
            {
                vehicleList.Add(vehicle);
                return $"New vehicle has been added: {vehicle.GetVehicleInfo()}";
            }
        }
        public static string RemoveVehicle(string serialNumber)
        {
            if (!(vehicleList.Any(k => k.VehicleSerialNumber == serialNumber)))
            {
                return "This vehicle is not in our database.";
            }
            else
            {
                Vehicle vehicleToRemove = vehicleList.Where(x => x.VehicleSerialNumber == serialNumber).SingleOrDefault();
                vehicleList.Remove(vehicleToRemove);
                return "Vehicle successfully deleted!";
            }
        }
        public static string UpdateVehicle(string serialNumber, string changeProp, string newValue)
        {
            if (!(vehicleList.Any(k => k.VehicleSerialNumber == serialNumber)))
            {
                return "This vehicle is not in our database.";
            }
            else
            {
                Vehicle vehicleToUpdate = vehicleList.Where(x => x.VehicleSerialNumber == serialNumber).SingleOrDefault();
                if (changeProp == "color")
                {
                    vehicleToUpdate.VehicleColor = newValue;
                }
                else if (changeProp == "model")
                {
                    vehicleToUpdate.VehicleModel = newValue;
                }  
                return $"Vehicle successfully changed! {vehicleToUpdate.GetVehicleInfo()}";
            }
        }
        public static string ListVehicles(string serialNum = "")
        {
            if (vehicleList.Count == 0)
            {
                return "There are no vehicles in our database.";
            }
            else
            {
                if (serialNum != "")
                {
                    if (vehicleList.Any(x => x.VehicleSerialNumber == serialNum))
                    {
                        Vehicle vehicleToList = vehicleList.Where(x => x.VehicleSerialNumber == serialNum).SingleOrDefault();
                        return vehicleToList.GetVehicleInfo();
                    }
                    else
                    {
                        return "This serial number does not exist.";
                    }                
                }
                StringBuilder listVehicles = new StringBuilder();
                foreach (var vehicle in vehicleList)
                {
                    listVehicles.AppendLine(vehicle.GetVehicleInfo());
                }
                return listVehicles.ToString().TrimEnd();
            }
        }
    }
}
