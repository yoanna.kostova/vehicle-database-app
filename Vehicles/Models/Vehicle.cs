﻿using System;

namespace Madin_Challenge___Vehicles.Models
{
    public abstract class Vehicle
    {
        //Fields
        private string model;
        private string color;
        private string serialNumber;

        //Constructor
        public Vehicle(string model, string color)
        {
            this.VehicleColor = color;
            this.VehicleModel = model;

            this.VehicleSerialNumber = GenerateSerialNumber();

        }

        //Properties
        public string VehicleModel
        {
            get
            {
                return this.model;
            }
            set
            {
                if (value ==null)
                {
                    throw new ArgumentNullException("Please provide a valid model");
                }
                this.model = value;
            }
        }
        public string VehicleColor
        {
            get
            {
                return this.color;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Please provide a valid color");
                }
                this.color = value;
            }
        }
        public string VehicleSerialNumber
        {
            get
            {
                return this.serialNumber;
            }
            private set
            {
                this.serialNumber = value;
            }
        }

        //Methods
        public string GetVehicleInfo()
        {
            return $"{this.VehicleSerialNumber} | {this.VehicleModel} {this.VehicleColor}";
        }
        private string GenerateSerialNumber()
        {
            bool[] check = new bool[100001];
            Random r = new Random();

            int num = r.Next(1, 100000);

            while (check[num] == true)
            {
                num = r.Next(1, 100000);
            }

            check[num] = true;
            return num.ToString(); ;
        }
    }
}
