﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles
{
    public class Actions
    {
        //Fields
        private List<Vehicle> vehicleList;
        private string vehicle;

        //Constructor
        public string Category(string vehicle)
        {
            throw new NotImplementedException();
        }

        //Methods
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicleList.Contains(vehicle))
            {
                throw new DuplicateWaitObjectException("This car already exiists in our database.");
            }
            else
            {
                vehicleList.Add(vehicle);
            }
        }
        public void RemoveCar(Vehicle vehicle)
        {
            if (!(vehicleList.Contains(vehicle)))
            {
                throw new DuplicateWaitObjectException("This car is not in our database.");
            }
            else
            {
                vehicleList.Remove(vehicle);
            }
        }
        public string Print(string command)
        {
            throw new NotImplementedException();

        }
    }
}
