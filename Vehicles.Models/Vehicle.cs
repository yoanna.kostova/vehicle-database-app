﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles
{
    public class Vehicle
    {
        //Fields
        private string color;
        private string model;
        private int serialNumber = 1;

        //Constructor
        public Vehicle(string model, string color)
        {
            this.VehicleModel = model;
            this.VehicleColor = color;
        }
        //Properties
        public string VehicleModel
        {
            get
            {
                return this.model;
            }
            set
            {
                if (this.model == null)
                {
                    return "Please add a valid model.";
                }
                else
                {
                    this.model = value;
                }
            }
        }
        public string VehicleColor
        {
            get
            {
                return this.color;
            }
            set
            {
                if (this.color == null)
                {
                    return "Please add a valid color.";
                }
                else
                {
                    this.color = value;
                }
            }
        }
        public int VehicleSerialNumber
        {
            get
            {
                return this.serialNumber;
            }
            set
            {
                this.serialNumber = value;
            }
        }
        //Methods
        //TODO: To print a certain vehicle
        public string GetVehicleInfo()
        {
            throw new NotImplementedException();
        }
        //TODO: To implement random serial number generator.
        private int SerialNumberGenerator()
        {
            throw new NotImplementedException();
        }
    }
}
