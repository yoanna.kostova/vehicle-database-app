﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Madin_Challenge___Vehicles
{
    public class Boat : Vehicle
    {
        public Boat(string model, string color, int serialNumber) : base(model, color, serialNumber)
        {
        }
    }
}
